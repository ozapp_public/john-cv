# john-cv
John online resume built with Nuxt, Contentful and ipfs hosting

## Docker Setup

One of the quickest ways to get up and running on your machine is by using Docker:
```
docker run --name nuxt-contentful-ipfs -v $(pwd):/src/app \
           -it -p 3000:3000 node:14 bash
```

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

## publish to ipfs
$ yarn ipfs
```

Set up example contentful [in this blog post](https://www.netlify.com/blog/2020/04/20/create-a-blog-with-contentful-and-nuxt)

For detailed explanation on how NuxtJs work, check out [Nuxt.js docs](https://nuxtjs.org).

For detailed explanation on how ipfs-deploy work, check out [ipfs-deploy](https://github.com/ipfs-shipyard/ipfs-deploy).

